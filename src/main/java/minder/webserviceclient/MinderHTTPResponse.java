package minder.webserviceclient;

import java.util.HashMap;

/**
 * Created by melis on 17/09/15.
 */
public class MinderHTTPResponse {
    public MinderHTTPResponse(){
        this.headers = new HashMap<String,String>();
    }

    private HashMap<String,String> headers;
    private String body;
    private int returnCode;

    public int getReturnCode() {
        return returnCode;
    }

    public HashMap<String, String> getHeaders() {
        return headers;
    }

    public String getBody() {
        return body;
    }

    public void setHeaders(HashMap<String, String> headers) {
        this.headers = headers;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }
}
