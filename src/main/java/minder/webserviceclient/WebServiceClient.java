package minder.webserviceclient;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.List;

/**
 * Created by melis on 17/09/15.
 */
@SpringBootApplication
public class WebServiceClient {
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(WebServiceClient.class);

    public static ClientHttpRequest createRequest(String command, HttpMethod httpMethod) {
        ClientHttpRequest clientHttpRequest = null;
        try {
            RestTemplate restTemplate = new RestTemplate();
            clientHttpRequest = restTemplate.getRequestFactory().createRequest(URI.create(command), httpMethod);

        } catch (IOException e) {
            throw new RuntimeException("Could not create HTTP Request! " + command, e);
        }

        return clientHttpRequest;
    }

    public static void addHeader(ClientHttpRequest clientHttpRequest,String headerName, String headerValue) {
        clientHttpRequest.getHeaders().add(headerName,headerValue);
    }

    public static void addBody(ClientHttpRequest clientHttpRequest,String bodyStr) {
        try {
            clientHttpRequest.getBody().write(bodyStr.getBytes());
        }
        catch (IOException e) {
                throw new RuntimeException("Could not write the request body: " + bodyStr, e);
        }
    }


    public static String getResponseBody(ClientHttpResponse clientHttpResponse){
        InputStream inputStream = null;
        try {
            inputStream = clientHttpResponse.getBody();
        } catch (IOException e) {
            throw new RuntimeException("Could not receive body of the response: ", e);
        }

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = null;
        String body = "";
        try {
            while (null != (line = bufferedReader.readLine())) {
                body += line + "\n";
            }
        } catch (IOException e) {
            throw new RuntimeException("Could not read line from input stream of response body: ", e);
        }

        return body;
    }

    public static int getRawStatusCode(ClientHttpResponse clientHttpResponse){
        int status = 0;
        try {
            status = clientHttpResponse.getRawStatusCode();
        } catch (IOException e) {
            throw new RuntimeException("Could get raw status code from response body: ", e);
        }

        return status;
    }

}
