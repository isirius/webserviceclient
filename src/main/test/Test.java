import minder.webserviceclient.MinderHTTPResponse;
import minder.webserviceclient.WebServiceClient;
import org.springframework.core.env.SystemEnvironmentPropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;

import javax.jws.WebService;
import java.util.List;

/**
 * Created by melis on 20/09/15.
 */
public class Test {
    public static void main(String [] args) throws Exception {
        //String getCommand = "http://130.206.118.4:8080/cipa-smp-full-webapp/iso6523-actorid-upis::0088:5798000000003";
        //String getCommand = "http://www.webservicex.net/globalweather.asmx/GetCitiesByCountry?CountryName=Deneme";
//String getCommand = "http://www.webservicex.net/globalweather.asmx/GetCitiesByCountry";
        //MinderHTTPResponse response = WebServiceClient.executePOSTCommand(getCommand);

        System.out.println("GET SAMPLE");
        String getCommand = "http://130.206.118.4:8080/cipa-smp-full-webapp/iso6523-actorid-upis::0088:5798000000003";
        ClientHttpRequest clientHttpRequest = WebServiceClient.createRequest(getCommand, HttpMethod.GET);
        clientHttpRequest.getHeaders().add("Content-Type","application/xml");

        ClientHttpResponse clientHttpResponse = clientHttpRequest.execute();
        String responseBody = WebServiceClient.getResponseBody(clientHttpResponse);
        System.out.println("Response Body: "+responseBody);
        int status = WebServiceClient.getRawStatusCode(clientHttpResponse);
        System.out.println("Response status: "+status);

        for (String key : clientHttpResponse.getHeaders().keySet()) {
            List<String> values = clientHttpResponse.getHeaders().get(key);
            System.out.println("Key: "+key+" value: "+ values.get(0));
        }

        System.out.println("POST SAMPLE");
        String postCommand = "http://localhost:9000/rest/getTestGroup";
        clientHttpRequest = WebServiceClient.createRequest(postCommand, HttpMethod.POST);
        clientHttpRequest.getHeaders().add("Content-Type","application/json");
        WebServiceClient.addBody(clientHttpRequest,"{\"id\":\"1\"}");

        clientHttpResponse = clientHttpRequest.execute();
        responseBody = WebServiceClient.getResponseBody(clientHttpResponse);
        System.out.println("Response Body: "+responseBody);
        status = WebServiceClient.getRawStatusCode(clientHttpResponse);
        System.out.println("Response status: "+status);

        for (String key : clientHttpResponse.getHeaders().keySet()) {
            List<String> values = clientHttpResponse.getHeaders().get(key);
            System.out.println("Key: "+key+" value: "+ values.get(0));
        }

    }
}
